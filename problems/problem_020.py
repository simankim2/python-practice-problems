# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

def has_quorum(attendees_list, members_list):
    # create an if statement checking if the value of attendees_list value
    # is greater than or equal to 50% of the members_list value
    if attendees_list >= members_list / 2:
        return True
    else:
        return False


print(has_quorum(10, 30))
