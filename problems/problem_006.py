# Complete the can_skydive function so that determines if
# someone can go skydiving based on these criteria
#
# * The person must be greater than or equal to 18 years old, or
# * The person must have a signed consent form

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_skydive(age, has_consent_form):
    # create an if statement checking if age is greater than or equal to 18
    if age >= 18:
        return print("You are eligible to skydive")
    if age < 18 and has_consent_form == True:
        return print("You have permission to skydive")
    else:
        return print("You are not eligible to skydive!")


can_skydive(15, False)
    # a return statement printing a response

    # another if statement checking if they have a signed consent form (bool)

    # another return statement printing a response
