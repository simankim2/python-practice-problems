# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# It uses the built-in function reversed and the join method
# for string objects.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def is_palindrome(word):
    # create a list of the letters in the reversed word using "reversed" function
    word_reversed = reversed(word)
    # then join the list into a string with the join method for string objects
    reversed_word = "".join(word_reversed)
    # an if statement checking it word is equal to reversed word u
    if reversed_word == word:
        return print(f"{word} is a palindrome!")
    else:
        return print(f"{word} is not a palindrome!")


is_palindrome("banana")
