# Complete the is_divisible_by_3 function to return the
# word "fizz" if the value in the number parameter is
# divisible by 3. Otherwise, just return the number.
#
# You can use the test number % 3 == 0 to test if a
# number is divisible by 3.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def is_divisible_by_3(number):
    # use modulo to see if number is divisible by 3
    if number % 3 == 0:
    # return a print statement to check if it is true or false and return
    # the appropriate response
        return "fizz"
    else:
        return number

# test the function
print(is_divisible_by_3(9))
