# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"

def gear_for_day(is_workday, is_sunny):
    # create an empty variable able to take in items needed for certain conditions
    gear_needed = []
    # check conditions using if statements
    # use if statement to check if day is not sunny and it is a workday
    if is_workday and not is_sunny:
        # add "umbrella" to the list
        gear_needed.append("umbrella")
    # if statement to check if it is a workday and add "laptop" if true
    if is_workday:
        gear_needed.append("laptop")
     # if statement to check if it is not a workday and add "surfboard" if true
    if not is_workday:
        gear_needed.append("surfboard")
    return gear_needed


print(gear_for_day(True, False))
