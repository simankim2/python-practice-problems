# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_make_pasta(ingredients):
    # create an if statement checking for flour, eggs, and oil in ingredients
    if (
        "flour" in ingredients
        and "eggs" in ingredients
        and "oil" in ingredients
    ):
        # return True if condition is fufilled, False if not
        return True
    else:
        return False



print(can_make_pasta(("flour", "eggs", "oil")))
