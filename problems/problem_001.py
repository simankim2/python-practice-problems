# Complete the minimum_value function so that returns the
# minimum of two values.
#
# If the values are the same, return either.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# creating a function that returns the minimum of two values
# need a for statement comparing value1 and value2

def minimum_value(value1, value2):
    if value1 < value2:
        return print(value1)
    else:
        return print(value2)


minimum_value(9, 5)
